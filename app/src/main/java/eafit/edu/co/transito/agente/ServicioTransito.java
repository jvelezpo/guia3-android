package eafit.edu.co.transito.agente;

import android.app.AlertDialog;
import android.os.StrictMode;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import eafit.edu.co.transito.NuevoActivity;

/**
 * Created by sebastian on 22/10/15.
 */
public class ServicioTransito {
    private final static String SERVICE_URL = "http://200.75.73.31/Transito2/TransitoService.svc";

    protected String ejecutarPOST(String operacion, String datos) throws ExepcionTransito {

        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
        String stringResp;
        int code;
        try {
            URL url = new URL(SERVICE_URL + operacion);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-type", "application/json");
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setChunkedStreamingMode(0);
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
            writer.write(datos);
            writer.flush();
            writer.close();
            code = conn.getResponseCode();
            String line;
            StringBuilder total = new StringBuilder();
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = rd.readLine()) != null) total.append(line);
            stringResp = total.toString();
        } catch (IOException e) {
            throw new ExepcionTransito("Falla servicio: " + e.getMessage());
        }
        if (code != HttpURLConnection.HTTP_OK)
            throw new ExepcionTransito("Fallo servicio (" + code + "): " + stringResp);
        return stringResp;
    }

    public boolean autenticarUsuario(String usuario, String clave) throws ExepcionTransito {

        JSONStringer datosReq;
        try {
            datosReq = new JSONStringer().object().key("Usuario").value(usuario).key("Clave").value(clave).endObject();
        } catch (JSONException e) {
            throw new ExepcionTransito("Falla datos de entrada: " + e.getMessage());
        }

        String stringResp = ejecutarPOST("/json/AutenticarUsuario", datosReq.toString());
        boolean resultado;
        try {
            JSONObject datosResp = new JSONObject(stringResp);
            resultado = datosResp.getBoolean("AutenticarUsuarioJSONResult");
        } catch (JSONException e) {
            throw new ExepcionTransito("Falla datos de respuesta: " + e.getMessage());
        }

        return resultado;
    }

    public ResultadoEnviar enviarComparendo(String placa, String motivo, int valor, String usuario, String ubicacion) throws ExepcionTransito {

        JSONStringer datosReq;
        try {
            datosReq = new JSONStringer()
                    .object()
                    .key("Placa").value(placa)
                    .key("Motivo").value(motivo)
                    .key("Valor").value(Integer.valueOf(valor).toString())
                    .key("Usuario").value(usuario)
                    .key("Ubicacion").value(ubicacion)
                    .endObject();
        } catch (JSONException e) {
            throw new ExepcionTransito("Fallo datos de entrada: " + e.getMessage());
        }
        String stringResp = ejecutarPOST("/json/EnviarComparendo", datosReq.toString());
        ResultadoEnviar resultado;
        try {
            JSONObject datosResp = new JSONObject(stringResp);
            resultado = new ResultadoEnviar();

            resultado.setExito(datosResp.getJSONObject("EnviarComparendoJSONResult").getBoolean("Exito"));

            resultado.setMensaje(datosResp.getJSONObject("EnviarComparendoJSONResult").getString("Mensaje"));
        } catch (JSONException e) {
            throw new ExepcionTransito("Fallo datos de respuesta: " + e.getMessage());
        }

        return resultado;
    }
}
