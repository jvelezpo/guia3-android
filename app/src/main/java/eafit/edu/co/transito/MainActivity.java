package eafit.edu.co.transito;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import eafit.edu.co.transito.agente.ExepcionTransito;
import eafit.edu.co.transito.agente.ServicioTransito;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onContinuarClick(View view) {
        // obtiene datos
        String usuario = ((TextView)findViewById(R.id.editUsuario)).getText().toString();
        String clave = ((TextView)findViewById(R.id.editClave)).getText().toString();

        // verifica el usuario
        ServicioTransito ws = new ServicioTransito();
        boolean valido = false;
        try {
            valido = ws.autenticarUsuario(usuario, clave);
        } catch (ExepcionTransito e) {
            new AlertDialog.Builder(this).setMessage(e.getMessage()).setPositiveButton("Ok", null).show();
            return;
        }

        if (!valido) {
            new AlertDialog.Builder(this).setMessage("El usuario o la contraseña son inválidos").setPositiveButton("Ok", null).show();
            return;
        }

        // guarda el usuario en sesion y redirige
        DatosSesion.usuario = usuario;
        Intent intent = new Intent(this, InicioActivity.class);
        startActivity(intent);
    }
}
