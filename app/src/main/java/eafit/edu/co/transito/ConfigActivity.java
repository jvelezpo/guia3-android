package eafit.edu.co.transito;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import eafit.edu.co.transito.agente.ExepcionTransito;
import eafit.edu.co.transito.agente.PersistenciaTransito;
import eafit.edu.co.transito.agente.Preferencia;

public class ConfigActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        loadConfig();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_config, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onGuardarClick(View view) {

        Preferencia preferencia = null;

        PersistenciaTransito persiste = new PersistenciaTransito();

        try {
            preferencia = persiste.ObtenerPreferencia(this);
        } catch (ExepcionTransito et) {
            new AlertDialog.Builder(this).setMessage(et.getMessage()).setPositiveButton("Ok", null).show();
            return;
        }

        CheckBox maxRegistros = (CheckBox)findViewById(R.id.maxRegistros);
        RadioGroup radioOrder = (RadioGroup) findViewById(R.id.radioOrder);

        int selectedId = radioOrder.getCheckedRadioButtonId();
        RadioButton radioSelected = (RadioButton) findViewById(selectedId);

        preferencia.setMax10(maxRegistros.isChecked());
        preferencia.setOrden(radioSelected.getText().toString());

        try {
            persiste.GuardarPreferencia(this, preferencia);
            new AlertDialog.Builder(this).setMessage("Configuracion guardada con exito").setPositiveButton("Ok", null).show();
        } catch (ExepcionTransito et) {
            new AlertDialog.Builder(this).setMessage(et.getMessage()).setPositiveButton("Ok", null).show();
            return;
        }
    }
    private void loadConfig(){

        Preferencia preferencia = null;

        PersistenciaTransito persiste = new PersistenciaTransito();

        try {
            preferencia = persiste.ObtenerPreferencia(this);
        } catch (ExepcionTransito et) {
            new AlertDialog.Builder(this).setMessage(et.getMessage()).setPositiveButton("Ok", null).show();
            return;
        }

        CheckBox maxRegistros = (CheckBox)findViewById(R.id.maxRegistros);
        RadioButton radioPlaca = (RadioButton) findViewById(R.id.radio_placa);
        RadioButton radioMotivo = (RadioButton) findViewById(R.id.radio_motivo);

        if(preferencia.isMax10()){
            maxRegistros.setChecked(true);
        } else {
            maxRegistros.setChecked(false);
        }

        if(preferencia.getOrden().equals("Placa")){
            radioPlaca.setChecked(true);
            radioMotivo.setChecked(false);
        } else if(preferencia.getOrden().equals("Motivo")){
            radioPlaca.setChecked(false);
            radioMotivo.setChecked(true);
        }
    }
}
