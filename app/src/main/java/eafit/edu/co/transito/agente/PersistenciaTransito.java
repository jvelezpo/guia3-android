package eafit.edu.co.transito.agente;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;

/**
 * Created by sebastian on 22/10/15.
 */
public class PersistenciaTransito {

    protected void escribeObjeto(Context context, String key, Object object) throws ExepcionTransito {
        try {
            FileOutputStream fos = context.openFileOutput(key, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(object);
            oos.close();
            fos.close();
        } catch (IOException e) {
            throw new ExepcionTransito("Falla escritura: " + e.getMessage());
        }
    }

    protected Object leeObjeto(Context context, String key) throws ExepcionTransito {
        try {
            FileInputStream fis = context.openFileInput(key);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object object = ois.readObject();
            return object;
        } catch (ClassNotFoundException e) {
            throw new ExepcionTransito("Falla lectura clase: " +
                    e.getMessage());
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            throw new ExepcionTransito("Falla lectura: " + e.getMessage());
        }
    }

    public Comparendo[] ObtenerComparendos(Context context) throws ExepcionTransito {

        Comparendo[] lista = (Comparendo[]) leeObjeto(context, "Comparendos");
        return lista != null ? lista : new Comparendo[0];
    }
    public void InsertarComparendo(Context context, Comparendo comparendo) throws ExepcionTransito {
        Comparendo[] lista = ObtenerComparendos(context);
        Comparendo[] nuevaLista = Arrays.copyOf(lista, lista.length + 1);
        nuevaLista[lista.length] = comparendo;
        escribeObjeto(context, "Comparendos", nuevaLista);
    }

    public Preferencia ObtenerPreferencia(Context context) throws ExepcionTransito {
        Preferencia pref = (Preferencia) leeObjeto(context, "Preferencia");
        return pref != null ? pref : new Preferencia();
    }
    public void GuardarPreferencia(Context context, Preferencia pref) throws ExepcionTransito {
        escribeObjeto(context, "Preferencia", pref);
    }

}
