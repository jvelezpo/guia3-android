package eafit.edu.co.transito.agente;

/**
 * Created by sebastian on 22/10/15.
 */
import java.io.Serializable;

public class Preferencia implements Serializable {

    private String orden;
    private boolean max10;

    public Preferencia(){
        setMax10(false);
        setOrden("Placa");
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    public boolean isMax10() {
        return max10;
    }

    public void setMax10(boolean max10) {
        this.max10 = max10;
    }
}