package eafit.edu.co.transito.agente;

/**
 * Created by sebastian on 22/10/15.
 */
public class ExepcionTransito extends Exception {
    public ExepcionTransito(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }
    public ExepcionTransito(String detailMessage) {
        super(detailMessage);
    }
}
