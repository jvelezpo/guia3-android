package eafit.edu.co.transito;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Date;

import eafit.edu.co.transito.agente.Comparendo;
import eafit.edu.co.transito.agente.ExepcionTransito;
import eafit.edu.co.transito.agente.PersistenciaTransito;
import eafit.edu.co.transito.agente.ResultadoEnviar;
import eafit.edu.co.transito.agente.ServicioTransito;

public class NuevoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nuevo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onEnviarClick(View view) {

        // obtiene datos
        EditText placa = (EditText)findViewById(R.id.editPlaca);
        String motivo = ((Spinner)findViewById(R.id.spinnerMotivo)).getSelectedItem().toString();

        EditText valor = (EditText) findViewById(R.id.editValor);
        int valorInt = Integer.parseInt(valor.getText().toString());

        if (placa.getText().toString().equals("") || valorInt <= 0) {
            new AlertDialog.Builder(this).setMessage("Datos invalidos").setPositiveButton("Ok", null).show();
            return;
        }

        // verifica el usuario
        ServicioTransito ws = new ServicioTransito();
        ResultadoEnviar valido = null;
        try {
            valido = ws.enviarComparendo(placa.getText().toString(), motivo, valorInt, DatosSesion.usuario, "Location");
        } catch (ExepcionTransito e) {
            new AlertDialog.Builder(this).setMessage(e.getMessage()).setPositiveButton("Ok", null).show();
            return;
        }

        if (!valido.isExito()) {
            new AlertDialog.Builder(this).setMessage("Error al guardar el comparendo").setPositiveButton("Ok", null).show();
            return;
        } else {
            new AlertDialog.Builder(this).setMessage("Registrado con exito").setPositiveButton("Ok", null).show();

            Comparendo comparendo = new Comparendo();
            comparendo.setPlaca(placa.getText().toString());
            comparendo.setMotivo(motivo);
            comparendo.setValor(valorInt);
            comparendo.setUsuario(DatosSesion.usuario);
            comparendo.setFecha(new Date());

            PersistenciaTransito persiste = new PersistenciaTransito();
            try {
                persiste.InsertarComparendo(this, comparendo);
            } catch (ExepcionTransito e) {
                new AlertDialog.Builder(this).setMessage(e.getMessage()).setPositiveButton("Ok", null).show();
                return;
            }
            placa.setText("");
            valor.setText("");
        }
    }
}
