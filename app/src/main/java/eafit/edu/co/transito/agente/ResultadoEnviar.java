package eafit.edu.co.transito.agente;

/**
 * Created by sebastian on 22/10/15.
 */
import java.io.Serializable;

public class ResultadoEnviar implements Serializable {
    private boolean exito;
    private String mensaje;

    public boolean isExito() {
        return exito;
    }

    public void setExito(boolean exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}