package eafit.edu.co.transito;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import eafit.edu.co.transito.agente.Comparendo;
import eafit.edu.co.transito.agente.ExepcionTransito;
import eafit.edu.co.transito.agente.PersistenciaTransito;

public class HistoriaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historia);

        cargarHistoria();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_historia, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void cargarHistoria() {
        PersistenciaTransito persiste = new PersistenciaTransito();
        Comparendo[] comparendos;
        try {
            comparendos = persiste.ObtenerComparendos(this);
        } catch (ExepcionTransito e) {
            new
                    AlertDialog.Builder(this).setMessage(e.getMessage()).setPositiveButton("Ok", null).show();
            return;
        }

        ArrayList<String> lista = new ArrayList<String>();
        for (int i = comparendos.length - 1; i >= 0; i--) {
            lista.add(comparendos[i].getPlaca() + ", " + comparendos[i].getMotivo() + ", " + comparendos[i].getValor());
        }

        ListView listHistoria = (ListView)findViewById(R.id.listHistoria);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lista);
        listHistoria.setAdapter(adapter);
    }
}
